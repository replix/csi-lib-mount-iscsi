# csi-lib-mount-iscsi package

The following operations are supported
* Login to iSCSI target and mount the block device if successful
  * if the fs type parameter is `block` then use *bind-mount* to expose the raw block device
  * connector parameters are saved in a local file
* Unmount the block device and logout from iSCSI target
  * connector parameters are retrieved from the file

Depends upon `csi-lib-iscsi/iscsiadm`.
