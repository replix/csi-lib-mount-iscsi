/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package iscsi

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"os"
	"path"
	"strconv"
	"strings"

	"github.com/golang/glog"
	"k8s.io/utils/mount"
	"k8s.io/utils/exec"
	"k8s.io/kubernetes/pkg/volume/util"

	iscsi_lib "gitlab.com/replix/csi-lib-iscsi/pkg/iscsiadm"
)

type iscsiDiskMounter struct {
	VolID         string
	TgtPortal     string
	Portals       []string
	Iqn           string
	Lun           int32
	InitiatorName string

	connector iscsi_lib.Connector

	readOnly     bool
	fsType       string
	mountOptions []string
	mounter      *mount.SafeFormatAndMount
	deviceUtil   util.DeviceUtil
	targetPath   string
}

type iscsiDiskUnmounter struct {
	VolID      string
	TargetPath string
	mounter    mount.Interface
	connector  *iscsi_lib.Connector
}

var (
	debug *log.Logger
)

func init() {
	// by default we don't log anything, EnableDebugLogging() can turn on some tracing
	debug = log.New(ioutil.Discard, "", 0)
}

// EnableDebugLogging provides a mechanism to turn on debug logging for this package
// output is written to the provided io.Writer
func EnableDebugLogging(writer io.Writer) {
	debug = log.New(writer, "DEBUG: ", log.Ldate|log.Ltime|log.Lshortfile)
	iscsi_lib.EnableDebugLogging(writer)
}

// MountParams - parameters for Mounter
type MountParams struct {
	TargetPath   string
	FsType       string
	MountOptions []string
	ReadOnly     bool
}

// GetDiskMounter - constructor for the iscsi disk Mounter
func GetDiskMounter(volID string, mountParams *MountParams,
	iscsiCtxt map[string]string) (*iscsiDiskMounter, error) {

	// targetPortal is mandatory, this is the main portal
	tgtPortal, ok := iscsiCtxt["targetPortal"]
	if !ok || tgtPortal == "" {
		return nil, fmt.Errorf("iSCSI target portal missing")
	}

	// IQN is mandatory
	iqn, ok := iscsiCtxt["iqn"]
	if !ok || iqn == "" {
		return nil, fmt.Errorf("iSCSI target IQN missing")
	}

	// default LUN is 0
	lun, ok := iscsiCtxt["lun"]
	if !ok || lun == "" {
		lun = "0"
	}

	// extra portals may be supplied
	var extraPortals []string
	extraPortalsStr, ok := iscsiCtxt["portals"]
	if ok && extraPortalsStr != "" {
		extraPortals = strings.Split(extraPortalsStr, " ")
	}

	// specific interface is optional
	iface, ok := iscsiCtxt["iscsiInterface"]
	if !ok {
		iface = ""
	}

	// initiatorName is optional
	initiatorName, ok := iscsiCtxt["initiatorName"]
	if !ok {
		initiatorName = ""
	}

	// chapDiscovery is off by default
	chapDiscovery := false
	chapDiscoveryStr, ok := iscsiCtxt["discoveryCHAPAuth"]
	if ok && chapDiscoveryStr == "true" {
		chapDiscovery = true
	}

	// authType is none by default
	authType, ok := iscsiCtxt["sessionCHAPAuth"]
	if !ok || authType == "" {
		authType = "none"
	}

	// default is no secrets
	secretParams, ok := iscsiCtxt["secret"]
	if !ok {
		secretParams = ""
	}

	// extra parsing

	// create portals list
	var allPortals []string
	// start with the leading tgt portal
	allPortals = append(allPortals, iscsiIPAddr(tgtPortal))
	// add all other portals
	for _, portal := range extraPortals {
		// accumulate portals string list
		allPortals = append(allPortals, iscsiIPAddr(portal))
	}

	// accumulate portals TargetInfo list for Connector
	connTargets := []iscsi_lib.TargetInfo{}
	for _, portal := range allPortals {
		host, port, _ := net.SplitHostPort(portal)
		connTargets = append(connTargets,
			iscsi_lib.TargetInfo{
				Iqn:    iqn,
				Portal: host,
				Port:   port,
			})
	}

	// get the numeric value of LUN
	var lunVal int32
	l, err := strconv.Atoi(lun)
	if err != nil {
		return nil, err
	}
	lunVal = int32(l)

	// parse secrets, if any
	sessionSecrets := iscsi_lib.Secrets{}
	discoverySecrets := iscsi_lib.Secrets{}
	if secretParams != "" {
		// first, create a map of chap-related params
		secret := parseSecret(secretParams)
		// extract the session secrets
		sessionSecrets, err = parseSessionSecrets(secret)
		if err != nil {
			return nil, err
		}
		// extract the discovery secrets
		discoverySecrets, err = parseDiscoverySecrets(secret)
		if err != nil {
			return nil, err
		}
	}

	return &iscsiDiskMounter{
		VolID:         volID,
		TgtPortal:     tgtPortal,
		Portals:       allPortals,
		Iqn:           iqn,
		Lun:           lunVal,
		InitiatorName: initiatorName,

		connector: iscsi_lib.Connector{
			Targets:          connTargets,
			Lun:              lunVal,
			AuthType:         authType,
			DiscoverySecrets: discoverySecrets,
			SessionSecrets:   sessionSecrets,
			Interface:        iface,
			Multipath:        len(allPortals) > 1,
			RetryCount:       120,
			CheckInterval:    1,
			DoDiscovery:      true,
			DoCHAPDiscovery:  chapDiscovery,
		},

		readOnly:     mountParams.ReadOnly,
		fsType:       mountParams.FsType,
		mountOptions: mountParams.MountOptions,
		mounter:      &mount.SafeFormatAndMount{
			Interface: mount.New(""),
			Exec:      exec.New(),
		},
		targetPath:   mountParams.TargetPath,
		deviceUtil:   util.NewDeviceHandler(util.NewIOHandler()),
	}, nil
}

// iscsiDiskMounter::Mount - log into iscsi target, attach, format and mount a LUN
func (mntr *iscsiDiskMounter) Mount() (string, error) {
	devicePathArr, err := iscsi_lib.Connect(mntr.connector)
	if err != nil {
		return "", err
	}
	if len(devicePathArr) == 0 {
		return "", fmt.Errorf("iscsi mount: connect reported success, but no path returned")
	}
	glog.Errorf("iscsi mount: connected at %v", devicePathArr)
	devicePath := devicePathArr[0]

	// Persist iscsi connector to json file for future Unmount
	persistFile := path.Join(path.Dir(mntr.targetPath), mntr.VolID+".json")
	glog.Errorf("iscsi mount: save connector to %s", persistFile)
	err = iscsi_lib.SaveConnectorToFile(&mntr.connector, persistFile)
	if err != nil {
		glog.Errorf("iscsi mount: failed to persist connection info: %v", err)
		glog.Errorf("disconnect volume and fail the publish request (because persistence files required for Unpublish)")
		return "", fmt.Errorf("unable to create persistence file for connection")
	}

	// Mount device
	mntPath := mntr.targetPath
	readOnly := mntr.readOnly
	fsType := mntr.fsType

	notMnt, err := mntr.mounter.IsLikelyNotMountPoint(mntPath)
	if err != nil && !os.IsNotExist(err) {
		return "", fmt.Errorf("iscsi mount: Heuristic determination of mount point failed:%v", err)
	}
	if !notMnt {
		glog.Infof("iscsi: %s already mounted", mntPath)
		return "", nil
	}

	var options []string
	if readOnly {
		options = append(options, "ro")
	} else {
		options = append(options, "rw")
	}

	if fsType != "block" { // mount a file system volume
		options = append(options, mntr.mountOptions...)

		if err := os.MkdirAll(mntPath, 0750); err != nil {
			glog.Errorf("iscsi mount: failed to mkdir %s, error", mntPath)
			return "", err
		}

		err = mntr.mounter.FormatAndMount(devicePath, mntPath, mntr.fsType, options)
		if err != nil {
			glog.Errorf("iscsi: failed to mount volume %s [%s] to %s, error %v",
				devicePath, mntr.fsType, mntPath, err)
		}
	} else { // bind mount a block device
		options = append(options, "bind")

		// Check if the target path exists. Create if not present.
		_, err = os.Lstat(mntPath)
		if os.IsNotExist(err) {
			if err = createFile(mntPath); err != nil {
				return "", fmt.Errorf("failed to create mount path: %s: %v", mntPath, err)
			}
		}
		if err != nil {
			return "", fmt.Errorf("failed to check if the target block file exists: %v", err)
		}

		if err = mntr.mounter.Mount(devicePath, mntPath, "", options); err != nil {
			return "", fmt.Errorf("failed to mount block device: %s at %s: %v",
				devicePath, mntPath, err)
		}
	}

	return devicePath, err
}

// GetDiskUnmounter - constructor for iscsi disk Unmounter
func GetDiskUnmounter(volID string, targetPath string) *iscsiDiskUnmounter {
	return &iscsiDiskUnmounter{
		VolID:      volID,
		TargetPath: targetPath,
		mounter:    mount.New(""),
	}
}

// iscsiDiskUnmounter::Unmount - unmount a volume and disconnect from iscsi target
func (umntr *iscsiDiskUnmounter) Unmount() error {
	targetPath := umntr.TargetPath
	mountAPI := umntr.mounter

	_, cnt, err := mount.GetDeviceNameFromMount(mountAPI, targetPath)
	if err != nil {
		glog.Errorf("iscsi unmount: failed to get device from mnt: %s\nError: %v",
			targetPath, err)
		return err
	}

	if pathExists, pathErr := mount.PathExists(targetPath); pathErr != nil {
		return fmt.Errorf("Error checking if path exists: %v", pathErr)
	} else if !pathExists {
		glog.Warningf("Warning: Unmount skipped because path does not exist: %v",
			targetPath)
		return nil
	}

	// load iscsi disk config from json file
	persistFile := path.Join(path.Dir(targetPath), umntr.VolID+".json")
	glog.Errorf("Restore connector from: %s", persistFile)
	umntr.connector, err = iscsi_lib.RestoreConnectorFromFile(persistFile)
	if err != nil {
		glog.Errorf("iscsi unmount: failed to get iscsi config from: %s Error: %v", persistFile, err)
		return err
	}

	if err = mountAPI.Unmount(targetPath); err != nil {
		glog.Errorf("iscsi unmount: failed for %s\nError: %v",
			targetPath, err)
		return err
	}
	cnt--
	if cnt != 0 {
		return nil
	}

	iscsi_lib.Disconnect(*umntr.connector)

	if err := os.RemoveAll(targetPath); err != nil {
		glog.Errorf("iscsi unmount: failed to remove mount path %s, Error: %v", targetPath, err)
		return err
	}

	if err := os.Remove(persistFile); err != nil {
		glog.Errorf("iscsi unmount: failed to remove iscsi config file %s, Error: %v", persistFile, err)
	}
	return nil
}

func iscsiIPAddr(addr string) string {
	if !strings.Contains(addr, ":") {
		addr = addr + ":3260"
	}
	return addr
}

func parseSecret(secretParams string) map[string]string {
	var secret map[string]string
	if err := json.Unmarshal([]byte(secretParams), &secret); err != nil {
		return nil
	}
	return secret
}

func parseSessionSecrets(secretParams map[string]string) (iscsi_lib.Secrets, error) {
	var ok bool
	secret := iscsi_lib.Secrets{}

	if len(secretParams) == 0 {
		return secret, nil
	}

	if secret.UserName, ok = secretParams["node.session.auth.username"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.session.auth.username not found in secret")
	}
	if secret.Password, ok = secretParams["node.session.auth.password"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.session.auth.password not found in secret")
	}
	if secret.UserNameIn, ok = secretParams["node.session.auth.username_in"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.session.auth.username_in not found in secret")
	}
	if secret.PasswordIn, ok = secretParams["node.session.auth.password_in"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.session.auth.password_in not found in secret")
	}

	secret.SecretsType = "chap"
	return secret, nil
}

func parseDiscoverySecrets(secretParams map[string]string) (iscsi_lib.Secrets, error) {
	var ok bool
	secret := iscsi_lib.Secrets{}

	if len(secretParams) == 0 {
		return secret, nil
	}

	if secret.UserName, ok = secretParams["node.sendtargets.auth.username"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.sendtargets.auth.username not found in secret")
	}
	if secret.Password, ok = secretParams["node.sendtargets.auth.password"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.sendtargets.auth.password not found in secret")
	}
	if secret.UserNameIn, ok = secretParams["node.sendtargets.auth.username_in"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.sendtargets.auth.username_in not found in secret")
	}
	if secret.PasswordIn, ok = secretParams["node.sendtargets.auth.password_in"]; !ok {
		return iscsi_lib.Secrets{}, fmt.Errorf("node.sendtargets.auth.password_in not found in secret")
	}

	secret.SecretsType = "chap"
	return secret, nil
}

func createDir(pathname string) error {
	err := os.MkdirAll(pathname, os.FileMode(0755))
	if err != nil {
		if !os.IsExist(err) {
			return err
		}
	}
	return nil
}

func createFile(pathname string) error {
	f, err := os.OpenFile(pathname, os.O_CREATE, os.FileMode(0644))
	defer f.Close()
	if err != nil {
		if !os.IsExist(err) {
			return err
		}
	}
	return nil
}
