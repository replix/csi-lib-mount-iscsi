/*
Copyright 2017 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"fmt"
	"os"

	iscsi_mnt "gitlab.com/replix/csi-lib-mount-iscsi/pkg/iscsi"
)

var (
	iqn       = flag.String("iqn", "iqn.2018-02.io.replix", "target IQN")
	tgtPortal = flag.String("portal", "127.0.0.1:3260", "target portal, ip:port")
	blockAccess = flag.Bool("block", false, "use raw block access instead of fs format and mount")
	doMount   = flag.Bool("mount", false, "perform mount")
	doUnmount = flag.Bool("unmount", false, "perform unmount")
)

func init() {
	flag.Set("logtostderr", "true")
}

func main() {
	flag.Parse()
	iscsi_mnt.EnableDebugLogging(os.Stdout)

	volID := "test-vol"
	fsType := "ext4"
	targetPath := "/tmp/test_vol"
	if *blockAccess {
		fsType = "block"
		targetPath = "/tmp/test_dev"
	}

	if !*doMount && !*doUnmount {
		*doMount = true
		*doUnmount = true
	}

	if *doMount {
		mntr, err := iscsi_mnt.GetDiskMounter(volID,
			&iscsi_mnt.MountParams{
				TargetPath: targetPath,
				FsType: fsType,
				ReadOnly: false,
			},
			map[string]string {
				"targetPortal": *tgtPortal,
				"iqn": *iqn,
				"lun": "1",
			})
		if err != nil {
			fmt.Println("failed to get iscsi mounter")
			return
		}
		devicePath, err := mntr.Mount()
		if err != nil {
			fmt.Println("failed to mount")
			return
		}
		fmt.Println("mounted", devicePath, "at", targetPath)
	}

	if *doUnmount {
		umntr := iscsi_mnt.GetDiskUnmounter(volID, targetPath)
		err := umntr.Unmount()
		if err != nil {
			fmt.Println("failed to unmount")
			return
		}
	}
}
