module gitlab.com/replix/csi-lib-mount-iscsi

go 1.13

require (
	github.com/container-storage-interface/spec v1.1.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/kubernetes-csi/csi-driver-iscsi v0.0.0-20200204021725-d8e3cc7bcd3d
	github.com/kubernetes-csi/csi-lib-iscsi v0.0.0-20190415173011-c545557492f4
	github.com/kubernetes-csi/csi-lib-utils v0.2.0
	github.com/spf13/cobra v0.0.3
	gitlab.com/replix/csi-lib-iscsi v0.0.0-20200317121312-fff0711807e3
	golang.org/x/net v0.0.0-20181113165502-88d92db4c548
	google.golang.org/grpc v1.16.0
	k8s.io/kubernetes v1.14.3
	k8s.io/utils v0.0.0-20200815180417-3bc9d57fc792
)
